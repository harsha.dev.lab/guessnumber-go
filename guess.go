/*
Created by Sriharsha Samala

A number between 1 and 100 is generated randomly
You have 10 tries to guess it correctly
*/

package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	seconds := time.Now().Unix()
	rand.Seed(seconds)
	target := rand.Intn(100) + 1
	fmt.Println("I've chosen a random number between 1 and 100")
	fmt.Println("Can you guess it?")
	// fmt.Println(target)

	reader := bufio.NewReader(os.Stdin)

	success := false

	for guesses := 0; guesses < 10; guesses++ {
		fmt.Println("You have ", 10-guesses, " left")
		fmt.Print("Make a guess: ")
		input, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		input = strings.TrimSpace(input)
		guess, err := strconv.Atoi(input)
		if err != nil {
			log.Fatal(err)
		}

		if guess < target {
			fmt.Println("Your guess is too LOW!")
		} else if guess > target {
			fmt.Println("Your guess is too HIGH!")
		} else {
			fmt.Println("Your guess is just RIGHT!")
			success = true
			break
		}
	}

	if !success {
		fmt.Println("Aww, you couldn't guess the number :(")
		fmt.Println("The number was ", target)
	}
}
